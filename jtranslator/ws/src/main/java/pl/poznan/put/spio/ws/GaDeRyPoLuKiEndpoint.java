package pl.poznan.put.spio.ws;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.poznan.put.spio.translator.GaDeRyPoLuKi;

import javax.jws.WebService;

// implementacja usługi WS
@WebService(serviceName = "GaDeRyPoLuKiEndpoint", portName = "GaDeRyPoLuKiPort",
        targetNamespace = "http://pl.poznan.put.spio/",
        endpointInterface = "pl.poznan.put.spio.ws.GaDeRyPoLuKiWs")
@Slf4j
@RequiredArgsConstructor
public class GaDeRyPoLuKiEndpoint implements GaDeRyPoLuKiWs {

    private final GaDeRyPoLuKi translator;

    @Override
    public Response translate(Request request) {
        log.info("ws translate request: {}", request.getMessage());
        String translated = translator.translate(request.getMessage());
        Response response = new Response();
        response.setTranslated(translated);
        response.setOrigin(request.getMessage());
        log.info("ws response {}", response);
        return response;
    }
}
