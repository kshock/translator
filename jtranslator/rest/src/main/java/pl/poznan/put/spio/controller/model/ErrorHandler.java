package pl.poznan.put.spio.controller.model;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView handleError(final Exception exception) {
        log.error("Holy crap!", exception);
        final ModelAndView mav = new ModelAndView();
        mav.setViewName("skucha");
        mav.addObject("exceptionType", exception.getClass().getName());
        mav.addObject("exception", exception.getMessage());
        return mav;
    }

}
